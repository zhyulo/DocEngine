#ifndef _LOG_H_
#define _LOG_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef LOG_LEVEL
#define LOG_LEVEL 5
#endif

/*
 * This is the local tag used for the following simplified
 * logging macros.  You can change this preprocessor definition
 * before using the other macros to change the tag.
 */
#ifndef LOG_TAG
#define LOG_TAG "LOG"
#endif

/*
 * Simplified macro to send a verbose log message using the current LOG_TAG.
 */
#ifndef LOGV
#if LOG_LEVEL > 4
#define LOGV(fmt, ...) fprintf(stdout, LOG_TAG "(V): " fmt "\n", ##__VA_ARGS__)
#else
#define LOGV(fmt, ...)
#endif
#endif

/*
 * Simplified macro to send a debug log message using the current LOG_TAG.
 */
#ifndef LOGD
#if LOG_LEVEL > 3
#define LOGD(fmt, ...) fprintf(stdout, LOG_TAG "(D): " fmt "\n", ##__VA_ARGS__)
#else
#define LOGD(fmt, ...)
#endif
#endif

/*
 * Simplified macro to send an info log message using the current LOG_TAG.
 */
#ifndef LOGI
#if LOG_LEVEL > 2
#define LOGI(fmt, ...) fprintf(stdout, LOG_TAG "(I): " fmt "\n", ##__VA_ARGS__)
#else
#define LOGI(fmt, ...)
#endif
#endif

/*
 * Simplified macro to send a warning log message using the current LOG_TAG.
 */
#ifndef LOGW
#if LOG_LEVEL > 1
#define LOGW(fmt, ...) fprintf(stderr, LOG_TAG "(W): " fmt "\n", ##__VA_ARGS__)
#else
#define LOGW(fmt, ...)
#endif
#endif

/*
 * Simplified macro to send an error log message using the current LOG_TAG.
 */
#ifndef LOGE
#if LOG_LEVEL > 0
#define LOGE(fmt, ...) fprintf(stderr, LOG_TAG "(E): " fmt "\n", ##__VA_ARGS__)
#else
#define LOGE(fmt, ...)
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif // _LOG_H_

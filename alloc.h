#ifndef __ALLOC_H_
#define __ALLOC_H_

#ifdef UseMemoryPool

#ifdef __cplusplus
extern "C" {
#endif

typedef struct block {
    char *avail;        // 内存块当前剩余内存起始地址
    char *end;          // 内存块终止地址
    struct block *next; // 下一个内存块地址
} Block;

typedef struct {
    Block *last; // 内存块链表的最末尾
    Block head;  // 内存块链表头结点
} *Heap, HeapEty;

#define malloc_ct(type, num) ((type*)HeapAllocate(MainHeap, sizeof(type) * (num)))
#define free_ct(buf)

extern Heap MainHeap;

void  InitHeap(Heap hp);
void* HeapAllocate(Heap hp, int size);
void  FreeHeap(Heap hp);
void  DestroyHeap(Heap hp);

#ifdef __cplusplus
}
#endif

#else

#include <stdlib.h>

#define malloc_ct(type, num) ((type*)malloc(sizeof(type) * (num)))
#define free_ct(buf) free(buf)

#endif

#endif

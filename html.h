#ifndef __HTML_H_
#define __HTML_H_

#include "xml.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    XmlItemListEty sub;
    XmlItem head;
    XmlItem body;
} *HtmlDoc, HtmlDocEty;

// 默认所有XmlItem、Input非空
XmlItem HtmlItemPause(Input in, XmlItem parent);

void HtmlPause(HtmlDoc doc, Input in);
void HtmlFree(HtmlDoc doc);
bool HtmlCharset(HtmlDoc doc, String charset);
bool HtmlTitle(HtmlDoc doc, String title);
void HtmlDiff(HtmlDoc doc, HtmlDoc doc2, XmlItemList lst);

#ifdef __cplusplus
}
#endif

#endif

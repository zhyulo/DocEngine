#ifndef __OUTPUT_H_
#define __OUTPUT_H_

#include "xml.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void WriteString(String str);

void WriteStrPtr(const char *str);
void WriteNewLine(int space);
void WriteStringMap(StringMap map);
void WriteXmlItem(XmlItem xml, int space);

#ifdef __cplusplus
}
#endif

#endif

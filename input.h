#ifndef __INPUT_H_
#define __INPUT_H_

#include "str.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    const char *ptr;
    int begin;
    int end;
} *Input, InputEty;

// 默认所有Input非空
Input InputFromFile(const char *path);
void InputClose(Input in);
void InputSkipBOM(Input in);
void InputSkipSpace(Input in);
void Input2String(Input in, String str);
void String2Input(String str, Input in);
void StringSetBegin(String str, Input in);
void StringSetEnd(String str, Input in);

#ifdef __cplusplus
}
#endif

#endif

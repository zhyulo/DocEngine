#ifndef __XML_H_
#define __XML_H_

#include "str.h"
#include "input.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct xml_item *XmlItem, XmlItemEty;
typedef struct {
    XmlItem *list;  // 标签列表
    int count;      // 标签个数
} *XmlItemList, XmlItemListEty;

struct xml_item {
    StringEty range;    // 标签数据范围
    StringEty name;     // 标签名
    StringMapEty attr;  // 标签属性
    StringEty val;      // 标签内容
    XmlItemListEty sub; // 子标签列表
    bool hasFreeVal;    // 是否包含无标签内容
    XmlItem parent;     // 父标签
};

typedef XmlItemListEty *XmlDoc, XmlDocEty;

void SetXmlStringEnd(String str);

// 默认XmlItemList非空
void XmlItemListAdd(XmlItemList lst, XmlItem xml);
XmlItem XmlItemListGet(XmlItemList lst, String name);
void XmlItemDiff(XmlItemList baseItems, XmlItemList newItems, XmlItemList diff);

// 默认所有XmlItem、Input非空
void XmlItemPauseAttr(XmlItem xml, Input in);
void XmlItemPauseNote(XmlItem xml, Input in);
void XmlItemPauseState(XmlItem xml, Input in);
XmlItem XmlItemPause(Input in, XmlItem parent);
void XmlItemFree(XmlItem xml);
XmlItem XmlItemSub(XmlItem xml, String name);
bool XmlItemValue(XmlItem xml, String val);
bool XmlItemEqual(XmlItem xml, XmlItem xml2);

// 默认XmlItemList、Input非空
void XmlPause(XmlDoc doc, Input in);
void XmlFree(XmlDoc doc);
bool XmlCharset(XmlDoc doc, String charset);
void XmlDiff(XmlDoc doc, XmlDoc doc2, XmlItemList lst);

#ifdef __cplusplus
}
#endif

#endif

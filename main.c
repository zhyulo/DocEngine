
#include "alloc.h"
#include "input.h"
#include "html.h"
#include "output.h"
#define LOG_TAG "TestMain"
#include "log.h"

void WriteString(String str)
{
    printf("%.*s", str->len, str->ptr);
}

int main(int argc, char *argv[])
{
    Input in = InputFromFile("test.html");
    if (in == NULL) return 1;
    Input in2 = InputFromFile("test2.html");
    if (in2 == NULL) {
        InputClose(in);
        return 1;
    }

    HtmlDocEty html, html2;
    HtmlPause(&html, in);
    HtmlPause(&html2, in2);

    XmlItemListEty diff;
    HtmlDiff(&html, &html2, &diff);
    int i;
    for (i = 0; i < diff.count; i++) {
        WriteXmlItem(diff.list[i], 0);
    }

    InputClose(in);
    InputClose(in2);

    #ifdef UseMemoryPool
    DestroyHeap(MainHeap);
    #endif
    return 0;
}

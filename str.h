#ifndef __STR_H_
#define __STR_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    const char *ptr;
    int len;
} *String, StringEty;

typedef struct {
    StringEty key;
    StringEty val;
} StringMapItem;

typedef struct {
    StringMapItem *items;
    int count;
} *StringMap, StringMapEty;

// 默认所有String非空
String StringFrom(const char *ptr);
void StringSet(String str, const char *ptr);
void StringClone(String str, String oldStr);
bool StringIsEmpty(String str);
void StringMove(String str, int dx);
int StringIndex(String str, char c);
int StringSubIndex(String str, String sub);
int StringLastIndex(String str, char c);
int StringLastSubIndex(String str, String sub);
bool StringBeginWith(String str, String begin);
bool StringEqualStr(String str, const char *str2);
bool StringEqual(String str, String str2);
bool StringCaseEqual(String str, String str2);
bool StringIgnoreCaseEqual(String str, const char *str2);

// 默认所有StringMap非空, String val可以为空
void StringMapPut(StringMap map, String key, String val);
String StringMapGet(StringMap map, String key);
bool StringMapEqual(StringMap map, StringMap map2);
void StringMapFree(StringMap map);

#ifdef __cplusplus
}
#endif

#endif
